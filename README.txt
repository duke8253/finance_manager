Description:
FinanceManager is a personal finance app for Android. It is used to keep track of expenses and incomes and display them in useful charts and graphs. Additionally, the app can scan receipts and automatically log the transaction. It also includes a handy tip and bill calculator.

Installation:
	From source code: Clone the repository and open the project in Android Studio. To run the project, open the Run menu option on Android Studio and select the option Run 'app'. The app can then be run on an emulator or a connected Android device in USB debug mode.

	From apk: Download the FinanaceManager.apk from the repository. Copy the file to the root of an Android device. Use an apk installer app to install the app.

Contributors:
Fei Deng (feideng2) (Leader)
Wutong Hao (whao2) (Leader)
Jane Wang (jzwang2)
David Chen (dxchen3)
Amit Murali Mohan (muralim2)
Edward Wu (ewu7)